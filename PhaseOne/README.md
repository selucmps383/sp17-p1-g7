#### _Copied and pasted from the pdf for quick reference in repo._
#### _Annotation Notes:_
*  **_(Complete)_** - This means minimum requirements are met. _It doesn't mean it can't be better though._
*  **_(Partial)_** - This means it is partially implemented but doesn't yet meet minimum requirements.

# Phase 1 Requirements
## Important Information

* There will be a code freeze at midnight Monday night, February 6 th .
* You will be presenting at random on Tuesday, February 7 th .
* Presentations are limited to 15 minutes. Be sure that you can present everything and still take questions at the end!
* If you have a PowerPoint presentation it should be no longer than 3 minutes long, and should only introduce your team and what each member did. We know what your requirements were.
* Very Important: ALL presentation materials MUST be committed to the repository.
* The solution should run as soon as it is pulled down from BitBucket!
* All students must be ready to show their code if asked to.

## Description

In this phase, you will be creating a web application using the ASP.NET Core framework.
The purpose of this phase is to get you familiar with the technologies you will be using for the
remainder of the semester. If you have any questions about how the technologies work, please
ask your Nick Dolan. If you need any more clarification, you can always email Envoc at
383@envoc.com.
You will be creating an inventory tracking application. In this application, there will be a
public page where users can enter their username and password to authenticate and sign in to
the portal. In the secure part of the portal, administrators should be able to add, edit, and delete
users and inventory items. There should be a public REST endpoint that allows users to make a
POST request and “purchase items” and a GET request that will allow them to see current
inventory levels. In addition, users should be able to go to a public page that will display all
current inventory levels for all items.


## Technology Needed
* Visual Studio 2015
* Microsoft SQL Server 2016
* ASP.NET Core
* Entity Framework Core using the Code First method
* BitBucket Account
  * All repository URLs will be in the format: `https://bitbucket.org/selucmps383/sp17-p1-g1`
  * Replace the last “1” with your group number
  * These will be set up soon, and an email will be sent out when they’re ready
  * All repositories will be made public after this phase is over
  * DO NOT commit your bin or obj folders.
  * Use this gitignore template

## Entities 

### User

* **_(Complete)_** Id
* **_(Complete)_** Username
* **_(Complete)_** First Name
* **_(Complete)_** Last Name
* **_(Complete)_** Password 

### InventoryItem

* **_(Complete)_** Id 
* **_(Partial)_** CreatedByUserId
* **_(Complete)_** Name
* **_(Complete)_** Quantity


## Technical Requirements

* **_(Complete)_** Database must be generated and seeded using Entity Framework, Code First migrations.
  * **_(Complete)_** When pulling your project down to present, we will run the update database
command to create your database - it cannot exist beforehand.
  * **_(Complete)_** Be sure to seed an “admin” user with the password “selu2017”
* **_(Complete)_** You are not to use the “default” ASP.NET Identity.
* Usernames must be unique (and enforced at the database level)
* **_(Complete)_** Passwords must be hashed
* **_(Partial)_** Verify the password that the user entered against the hash stored in the database (lookup by Username).
* Once the user enters the correct username/password, you can authenticate them using cookie middleware.
* **_(Partial)_** Only the public login section, the inventory level viewing page, and the purchasing endpoints may be accessible when not logged in (Use the Authorize attribute to secure your site).
* No Ids, passwords, or other “unfriendly” types of data that are irrelevant to the users of the site should appear on any web pages.


## User Stories

* As an administrator, I should be able to login using my username/password in order to access the back-end management portal.
* **_(Complete)_** As a user, I should be able to go to a public page without logging in, in order to see the current inventory levels.
* **_(Complete)_** As a user, I should be able to submit a GET request to an inventory endpoint, in order to retrieve inventory levels from the system.
* **_(Partial)_** As a user, I should be able to execute a POST request to an inventory endpoint, in order to purchase an item (and decrease the inventory level).
* **_(Partial)_** As an administrator, I should be able to list, add, edit, and delete users from the system.
* **_(Partial)_** As an administrator, I should be able to list, add, edit, and delete inventory items from the system.


## Critical Areas as of 04 February 2017:

### Admin User

* Get Login working correctly
	* Verify password and set CookieMiddleWare
	* Add management page
* Logout Functionality
* Stop passwords from being returned to view.
* Ensure unique Username


### InventoryItems

* Limit/Format what is returned to view
* Format json from API to exclude UserID that created the item
* Create "purchase" button that will decrement quantity only; For unauthenticated users.


### Frontend UI/UX

* Anything and everything can be made better

Most of the basic layout for this is already done, but it is all from the generated templates and is a bit ugly.

#### Pages and Content ####

What should be there, a lot of which already is. You just have to look at the code.

* Public/Home page
	* No authentication required.
	* Show inventory items with a detail button
	* Login button for admin users to log in.

* Inventory Items Detail page
	* No authentication required.
	* Show inventory item name and quantity.
	* Button to purchase, which would decrement the quantity.

* Admin/management portal
	* Authentication Required
	* All CRUD operations on Users.
	* All CRUD operations on InventoryItems.
	* Logout button.
