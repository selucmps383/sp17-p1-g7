﻿using PhaseOne.Models;
using Microsoft.EntityFrameworkCore;

namespace PhaseOne.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<InventoryItem> InventoryItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<InventoryItem>().ToTable("InventoryItem");
        }
    }
}
