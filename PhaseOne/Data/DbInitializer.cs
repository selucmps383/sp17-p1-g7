﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using CryptoHelper;
using PhaseOne.Data;

namespace PhaseOne.Models
{
    public class DbInitializer
    {

        public static void Initialize(DataContext context)
        {

            context.Database.EnsureCreated();

            if (context.Users.Any())
            {
                return;
            }

            var users = new User[]
            {
                new User {Username="admin",FirstName="Nicholas",LastName="Cage",Password=Crypto.HashPassword("selu2017")},
                new User {Username="billy147",FirstName="billy",LastName="Will",Password=Crypto.HashPassword("secret")}
            };

            foreach (User u in users)
            {
                context.Users.Add(u);
            }
            context.SaveChanges();

            var inventoryItems = new InventoryItem[]
            {
                new InventoryItem {Name="Bat",Quantity=10,UserID=1},
                new InventoryItem {Name="Ball",Quantity=20,UserID=2},
                new InventoryItem {Name="Glove",Quantity=15,UserID=2},
                new InventoryItem {Name="Shoes",Quantity=13,UserID=1}
            };

            foreach (InventoryItem i in inventoryItems)
            {
                context.InventoryItems.Add(i);
            }
            context.SaveChanges();

        }

    }
}
