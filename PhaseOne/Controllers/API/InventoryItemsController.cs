using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PhaseOne.Data;
using PhaseOne.Models;

namespace PhaseOne.Controllers.API
{
    [Produces("application/json")]
    [Route("api/InventoryItems")]
    public class InventoryItemsController : Controller
    {
        private readonly DataContext _context;

        public InventoryItemsController(DataContext context, InventoryItem item)
        {
            _context = context;
        }

        // GET: api/InventoryItems
        [HttpGet]
        public IEnumerable<InventoryItem> GetInventoryItems(InventoryItem item)
        {
            return _context.InventoryItems;
        }

        // GET: api/InventoryItems/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetInventoryItem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            InventoryItem inventoryItem = await _context.InventoryItems.SingleOrDefaultAsync(m => m.ID == id);

            if (inventoryItem == null)
            {
                return NotFound();
            }

            return Ok(inventoryItem);
        }

        // PUT: api/InventoryItems/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInventoryItem([FromRoute] int id, [FromBody] InventoryItem inventoryItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != inventoryItem.ID)
            {
                return BadRequest();
            }

            _context.Entry(inventoryItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InventoryItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/InventoryItems
        [HttpPost]
        public async Task<IActionResult> PostInventoryItem([FromBody] InventoryItem inventoryItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.InventoryItems.Add(inventoryItem);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (InventoryItemExists(inventoryItem.ID))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetInventoryItem", new { id = inventoryItem.ID }, inventoryItem);
        }

        // DELETE: api/InventoryItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInventoryItem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            InventoryItem inventoryItem = await _context.InventoryItems.SingleOrDefaultAsync(m => m.ID == id);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            _context.InventoryItems.Remove(inventoryItem);
            await _context.SaveChangesAsync();

            return Ok(inventoryItem);
        }

        private bool InventoryItemExists(int id)
        {
            return _context.InventoryItems.Any(e => e.ID == id);
        }
    }
}