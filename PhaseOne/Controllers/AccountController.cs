using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PhaseOne.Data;
using PhaseOne.Models;
using CryptoHelper;
using System.Security.Claims;
using PhaseOne.Controllers;

namespace PhaseOne.Controllers
{
    
    public class AccountController : Controller
    {
        private readonly DataContext _context;

        public AccountController(DataContext context)
        {
            _context = context;
        }

        // GET: account
        public ActionResult Index()
        {   
            if (HttpContext.Session.GetString("UserID") == null)
            {
                return RedirectToAction("Login");
            }
            return View();
        }


        [AllowAnonymous]
        public ActionResult Register()
        {
            if (HttpContext.Session.GetString("UserID") != null)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(User user)
        {
            if (ModelState.IsValid)
            {
                var duplicateUsernameFound = _context.Users.Where(u => u.Username == user.Username).FirstOrDefault();
                if (duplicateUsernameFound != null)
                {
                    ViewBag.Message = "This username already exists. You must enter a different username to register.";
                }
                else
                {
                    user.Password = Crypto.HashPassword(user.Password);
                    _context.Add(user);
                    _context.SaveChanges();

                    ModelState.Clear();
                    ViewBag.Message = user.FirstName + " " + user.LastName + " has successfully registered.";
                }
            }
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            if (HttpContext.Session.GetString("UserID") != null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(User user)
        {
            var account = _context.Users.Where(u => u.Username == user.Username).SingleOrDefault();
            if (account != null && Crypto.VerifyHashedPassword(account.Password, user.Password))
            {
                HttpContext.Session.SetString("UserID", account.ID.ToString());
                HttpContext.Session.SetString("Username", account.Username);
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("", "Invalid username and/or password.");
            }
            return View();
        }


        public ActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }
        

        //// GET: /Account/Login
        //[HttpGet]
        //[AllowAnonymous]
        //public ActionResult Login(string returnUrl = null)
        //{
        //    ViewData["ReturnUrl"] = returnUrl;
        //    return View();
        //}

        //// GET: Account
        //public ActionResult Index()
        //{
        //    return View();
        //}

        //// GET: Account/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        //// GET: Account/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Account/Create
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(IFormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Account/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: Account/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Account/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: Account/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}