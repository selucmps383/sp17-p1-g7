﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PhaseOne.Data;
using Microsoft.EntityFrameworkCore;

namespace PhaseOne.Controllers
{
    public class HomeController : Controller
    {
        private readonly DataContext _context;

        public HomeController(DataContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            ViewData["Message"] = "Public page.";
            return View(await _context.InventoryItems.ToListAsync());
        }

        public IActionResult Error()
        {
            return View();
        }        

    }
}
