﻿using Newtonsoft.Json;

namespace PhaseOne.Models
{
    public class InventoryItem
    {
        [JsonIgnore]
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }

        public User User { get; set; }
    }
}
