﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PhaseOne.Models
{
    // Be sure to seed an “admin” user with the password “selu2017”
    public class User
    {
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "You must provide a username.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "You must provide your first name.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "You must provide your last name.")]
        public string LastName { get; set; }

        [JsonIgnore]
        [Required(ErrorMessage = "You must provide a password.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
