# Warehouse and Inventory Tracking #
## ASP.NET Web App ##

### Phase One, Group 7 ###
CMPS 383, Spring 2017

# Inventory tracking application in ASP.NET Core framework
  
> Do NOT use "default" ASP.NET Identity.

> Hint: Use Authorize attribute to handle login-required pages vs public pages.
> Hint: There should be a public REST endpoint where users can make a POST request and "purchase items", and a GET request that will allow them to see current inventory levels

(1) Database
----
  - Generate and seed a database via Entity Framework.  This database should hold usernames, passwords, and inventory items.
  - Be sure to seed an "admin" user with password "selu2017"
  - Login usernames must be unique (enforced at the database level)

(2) Webpages
---
> Do not display any "unfriendly" types of data like IDs, passwords, or anything irrelevant to users of the site.
  - Public login webpage
  - Public (no login req.) inventory page
  - Once logged into the secure part of the portal, admins are able to add, edit, and delete users and inventory items.

(2) General Programming Stuff
---
  - Passwords must be hashed
  - User login verification done via database lookup by Username

(3) Authentification
---
  - Correct user login authentification using cookie middleware
